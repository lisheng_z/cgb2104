package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

@Component
@Data
@Accessors(chain = true)//开启链式加载
@TableName("demo_user")//实现对象与表名的映射
public class User {
    //设定主键自增
    @TableId(type = IdType.AUTO)
    private Integer id;
    //@TableField("name") 属性与字段名相同可省略
    private String name;
    //@TableField("age")
    private Integer age;
    //@TableField("sex")
    private String sex;
}
