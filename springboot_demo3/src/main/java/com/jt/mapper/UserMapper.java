package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import java.util.List;

//@Mapper //Spring为该接口创建一个代理对象
//继承接口之后，必须添加范型对象  否则程序无法执行
public interface UserMapper extends BaseMapper<User> {

    List<User> findAll();

}
