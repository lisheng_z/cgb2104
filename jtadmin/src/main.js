import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import './assets/css/global.css'
import './assets/ali-icon/iconfont.css'
/* 导入富文本编辑器 */
import VueQuillEditor from 'vue-quill-editor'

/* 导入富文本编辑器对应的样式 */
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

/* 导入axios包 */
import axios from 'axios'
/* 设定axios的请求根目录 */
// axios.defaults.baseURL = 'http://localhost:8091/'

//前端访问后端通过域名的方式访问
axios.defaults.baseURL = 'http://manage.jt.com/'


/* 向vue对象中添加全局对象 以后发送ajax请求使用$http对象 */
Vue.prototype.$http = axios

Vue.config.productionTip = false

/* 定义过滤器 */
Vue.filter("priceFormat",function(price){

    return (price / 100).toFixed(2)
})

/* 将富文本编辑器注册为全局可用的组件 */
Vue.use(VueQuillEditor)
/* Vue.prototype 父子组件参数传递 属性/对象传值
   Vue.use(VueQuillEditor) 父子组件插件传递
 */


new Vue({
  /* 定义路由对象*/
  router,
  /* 绑定页面*/
  render: h => h(App)
}).$mount('#app') /* 绑定页面中的div元素 */

/* 下边的2行代码 类似于html代码中的  el: "#app" */
