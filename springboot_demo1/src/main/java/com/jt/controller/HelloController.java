package com.jt.controller;

import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//Spring指定配置文件加载
@PropertySource(value = "classpath:/user.properties",
                encoding = "utf-8")
public class HelloController {

    /**
     * 表达式：Springel表达式 简称为spel表达式
     * 语法：${表达式内容}
     * 工作原理：
     *      容器：在内存中一个存储大量数据的Map集合
     *      1，当SpringBoot程序启动时，首先加载application.yml的配置文件
     *      2，当程序加载key-value结构时，将数据保存到Map集合中(容器内部)
     *      3，利用Spel表达式，通过key，获取value，之后为属性赋值
     */
    @Value("${userinfo.name}")
    String name;
    @Value("${user.info2}")
    String name2;

    @RequestMapping("hello")
    public String hello(){
        String str ="欢迎使用SpringBoot";
        System.out.println(str+name);
        return str+name+name2;
    }

    @RequestMapping("getUser")
    public User getUser(){
        User user = new User();
        user.setId(1).setName("laiyingcheng").setAge(22).setSex("nan");
        return user;
    }

}

