package com.jt.service;

import com.jt.pojo.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    int deletById(Integer id);

    int updataById(User user);

    User getUserById(Integer id);

    List<User> getUserByNS(User user);

    List<User> getUserByLikeS(String name);

    int deleteUserByName(User user);

    int deleteUserByNS(User user);

    void deleteUserById(Integer id);

    void saveUser(User user);

    void updateUser(User user);
}
