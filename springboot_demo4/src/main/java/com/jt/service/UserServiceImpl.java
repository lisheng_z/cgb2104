package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }

    @Override
    public int deletById(Integer id) {
        return userMapper.deleteById(id);
    }

    @Override
    public int updataById(User user) {
        return userMapper.updateById(user);
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.selectById(id);
    }


    //MP 以对象的方式操作数据库
    @Override
    public List<User> getUserByNS(User user) {
        //根据对象不为null的属性充当where条件
        QueryWrapper queryWrapper = new QueryWrapper(user);
        //QueryWrapper queryWrapper = new QueryWrapper();
        //queryWrapper.eq("name", user.getName());
        return userMapper.selectList(queryWrapper);
    }

    @Override
    public List<User> getUserByLikeS(String name) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.likeLeft("name", name);
        return userMapper.selectList(queryWrapper);
    }

    //Sql:delete from demo_user
    @Override
    public int deleteUserByName(User user) {
        //根据对象中不为null的属性当作where条件
        return userMapper.delete(new QueryWrapper(user));
    }

    @Override
    public int deleteUserByNS(User user) {
        QueryWrapper queryWrapper = new QueryWrapper(user);
        return userMapper.delete(queryWrapper);
    }

    @Override
    public void deleteUserById(Integer id) {
        userMapper.deleteById(id);
    }

    @Override
    public void saveUser(User user) {
        userMapper.insert(user);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateById(user);
    }

}
