package com.jt.controller;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("itemCat")
public class ItemCatController {

    @Autowired
    private ItemCatService itemCatService;

    @GetMapping("findItemCatList/{level}")
    public SysResult findItemCatList(@PathVariable Integer level){
        return SysResult.success(itemCatService.findItemCatList(level));
    }

    @PutMapping("status/{id}/{status}")
    @Transactional
    public SysResult updateStatus(ItemCat itemCat){
        return itemCatService.updateStatus(itemCat) != 0 ? SysResult.success() : SysResult.fail();
    }

    @PostMapping("saveItemCat")
    @Transactional
    public SysResult saveItemCat(@RequestBody ItemCat itemCat){
        return itemCatService.saveItemCat(itemCat) !=0 ? SysResult.success() : SysResult.fail();
    }

    @DeleteMapping("deleteItemCat")
    @Transactional
    public  SysResult deleteItemCat(ItemCat itemCat){
        return itemCatService.deleteItemCat(itemCat) != 0 ? SysResult.success() : SysResult.fail();
    }

    @PutMapping("updateItemCat")
    public SysResult updateItemCat(@RequestBody ItemCat itemCat){
        return itemCatService.updateItemCat(itemCat) != 0 ? SysResult.success() : SysResult.fail();
    }

}
