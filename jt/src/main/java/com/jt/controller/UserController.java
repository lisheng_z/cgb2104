package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

//    @GetMapping("findAll")
//    public List<User> findAll(){
//        return userService.findAll();
//    }

    @PostMapping("login")
    public SysResult login(@RequestBody User user){
        String token = userService.login(user);
        if(StringUtils.hasLength(token)){
            return SysResult.success(token);
        }
        return SysResult.fail();
    }

    @GetMapping("list")
    public SysResult getUserList(PageResult pageResult){
        pageResult = userService.getUserList(pageResult);
        return SysResult.success(pageResult);
    }

    @PutMapping("status/{id}/{status}")
    @Transactional
    public SysResult updataStatus(User user){
        return userService.updataStatus(user) ? SysResult.success() : SysResult.fail();
    }

    @PostMapping("addUser")
    @Transactional
    public SysResult addUser(@RequestBody User user){
        return userService.addUser(user) ? SysResult.success() : SysResult.fail();
    }

    @GetMapping("{id}")
    public SysResult getUserById(@PathVariable Integer id){
        User user = userService.getUserById(id);
        if (user != null){
            return SysResult.success(user);
        }
        return SysResult.fail();
    }

    @PutMapping("updateUser")
    @Transactional
    public SysResult updateUser(@RequestBody User user){
        return userService.updateUser(user) ? SysResult.success() : SysResult.fail();
    }

    @DeleteMapping("{id}")
    @Transactional
    public SysResult deleteUser(@PathVariable Integer id){
        return userService.deleteUser(id) ? SysResult.success() : SysResult.fail();
    }

}