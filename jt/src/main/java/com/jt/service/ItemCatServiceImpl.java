package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemCatServiceImpl implements ItemCatService{

    @Autowired
    private ItemCatMapper itemCatMapper;

    public Map<Integer,List<ItemCat>> itemCatMap(){
        //1.定义Map集合
        Map<Integer,List<ItemCat>> map = new HashMap<>();
        //2.查询所有的数据库信息 1-2-3
        List<ItemCat> list = itemCatMapper.selectList(null);
        for(ItemCat itemCat :list){
            int parentId = itemCat.getParentId();//获取父级ID
            if(map.containsKey(parentId)){ //判断集合中是否已经有parentId
                //有key  获取list集合 将自己追加到集合中
                List<ItemCat> exeList = map.get(parentId);//引用对象
                exeList.add(itemCat);
            }else{
                //没有key,将自己封装为第一个list元素
                List<ItemCat> firstList = new ArrayList<>();
                firstList.add(itemCat);
                map.put(parentId,firstList);
            }
        }return map;
    }

    public void getList(Map<Integer,List<ItemCat>> map,List<ItemCat> list,Integer level){
        if(level > 1){
            for (ItemCat itemCat : list){
                List<ItemCat> itemCatList = map.get(itemCat.getId());
                if(itemCatList == null){
                    continue;
                }
                getList(map, itemCatList, level-1);
                itemCat.setChildren(itemCatList);
            }
        }
    }

    /*@Override
    public List<ItemCat> findItemCatList(Integer level) {
        long startTime = System.currentTimeMillis();
        Map<Integer,List<ItemCat>> map = itemCatMap();
        //1.如果level=1 说明获取一级商品分类信息 parent_id=0
        if(level == 1){
            return map.get(0);
        }

        if(level == 2){ //获取一级和二级菜单信息
            return getTwoList(map);
        }

        //3.获取三级菜单信息
        //3.1获取二级商品分类信息  BUG:有的数据可能没有子级 如何处理
        List<ItemCat> oneList = getTwoList(map);
        for (ItemCat oneItemCat : oneList){
            //从一级集合中,获取二级菜单列表
            List<ItemCat> twoList = oneItemCat.getChildren();
            if(twoList == null){
                continue;
            }
            for(ItemCat twoItemCat : twoList){
                //查询三级商品分类  条件:parentId=2级ID
                List<ItemCat> threeList = map.get(twoItemCat.getId());
                twoItemCat.setChildren(threeList);
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("耗时:"+(endTime - startTime)+"毫秒");
        return oneList;
    }

    public List<ItemCat> getTwoList(Map<Integer,List<ItemCat>> map){
        List<ItemCat> oneList = map.get(0);
        for (ItemCat oneItemCat : oneList){//查询二级 parentId=1级Id
            List<ItemCat> twoList = map.get(oneItemCat.getId());
            oneItemCat.setChildren(twoList);
        }
        //二级嵌套在一级集合中,所有永远返回的都是1级.
        return oneList;
    }*/
    /**
    * 方法二
    *
    *    @Override
    *   public List<ItemCat> findItemCatList(Integer level) {
    *       long start = System.currentTimeMillis();
    *       List<ItemCat> list = itemCatMapper.selectList(null);
    *       HashMap<Integer,ItemCat> map = new HashMap<>();
    *       List<ItemCat> oneList = new ArrayList<>();
    *       for(ItemCat oneItemCat : list){
    *                   map.put(oneItemCat.getId(), oneItemCat);
    *       }
    *       for (ItemCat itemCat : list) {
    *           if(itemCat.getParentId() == 0){
    *               oneList.add(itemCat);
    *           }else {
    *               if(map.get(itemCat.getParentId()).getChildren() == null){
    *                   map.get(itemCat.getParentId()).setChildren(new ArrayList<ItemCat>());
    *               }
    *               map.get(itemCat.getParentId()).getChildren().add(itemCat);
    *           }
    *       }
    *       long end = System.currentTimeMillis();
    *       System.out.println("业务完成用时："+(end - start)+"ms");
    *       return oneList;
    *   }
    **/

    /**
     * 方法一
     *
     *    @Override
     *   public List<ItemCat> findItemCatList(Integer level) {
     *       long start = System.currentTimeMillis();
     *       QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
     *       queryWrapper.eq("parent_id", 0);
     *       List<ItemCat> oneList = itemCatMapper.selectList(queryWrapper);
     *       for(ItemCat oneItemCat : oneList){
     *           queryWrapper.clear();
     *           queryWrapper.eq("parent_id", oneItemCat.getId());
     *           List<ItemCat> twoList = itemCatMapper.selectList(queryWrapper);
     *           for(ItemCat twoItemCat : twoList){
     *               queryWrapper.clear();
     *               queryWrapper.eq("parent_id", twoItemCat.getId());
     *               List<ItemCat> threeList = itemCatMapper.selectList(queryWrapper);
     *               twoItemCat.setChildren(threeList);
     *           }
     *           oneItemCat.setChildren(twoList);
     *       }
     *       long end = System.currentTimeMillis();
     *       System.out.println("业务完成用时："+(end - start)+"ms");
     *       return oneList;
     *   }
     *
     **/
    /**
     * 方法三
     * 思路：仅查询数据库一次，将itemCat 存储到Map<Integr,List<ItemCat>> 使用递归函数
     * @param level
     * @return
     **/
    @Override
    public List<ItemCat> findItemCatList(Integer level) {
        long start = System.currentTimeMillis();
        Map<Integer, List<ItemCat>> map = itemCatMap();
        List<ItemCat> oneList = map.get(0);
        getList(map, oneList, level);
        long end = System.currentTimeMillis();
        System.out.println("业务完成用时："+(end - start)+"ms");
        return oneList;
    }

    @Override
    public Integer updateStatus(ItemCat itemCat) {
        return itemCatMapper.updateById(itemCat);
    }

    @Override
    public Integer saveItemCat(ItemCat itemCat) {
        return itemCatMapper.insert(itemCat);
    }

    @Override
    public Integer deleteItemCat(ItemCat itemCat) {
        //1.判断是否为三级菜单
        if(itemCat.getLevel() == 3){
            //如果是三级.则直接删除
            return itemCatMapper.deleteById(itemCat.getId());
        }
        //2.检查菜单是否为二级
        if(itemCat.getLevel() == 2){
            //应该先删除三级 根据parent_id= 2级ID即可.
            QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id", itemCat.getId());
            itemCatMapper.delete(queryWrapper);
            //再删除2级菜单
            return itemCatMapper.deleteById(itemCat.getId());
        }

        /*//1.判断是否为三级菜单
        if(itemCat.getLevel() == 3){
            //如果是三级.则直接删除
            itemCatMapper.deleteById(itemCat.getId());
            return; //程序终止.
        }

        //2.检查菜单是否为二级
        if(itemCat.getLevel() == 2){
            //应该先删除三级 根据parent_id= 2级ID即可.
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("parent_id",itemCat.getId());
            itemCatMapper.delete(queryWrapper);
            //再删除2级菜单
            itemCatMapper.deleteById(itemCat.getId());
            return;
        }*/

        //删除1级商品分类信息  先删除3级.再删除2级.
        //1.首先获取商品分类2级信息 parent_id= 1级ID
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("parent_id",itemCat.getId());
        List<ItemCat> twoList = itemCatMapper.selectList(queryWrapper);
        for (ItemCat twoItemCat : twoList){
            queryWrapper.clear();
            queryWrapper.eq("parent_id",twoItemCat.getId());
            itemCatMapper.delete(queryWrapper);//删除3级菜单
            itemCatMapper.deleteById(twoItemCat.getId());//删除2级
        }
        //删除1级.
        return itemCatMapper.deleteById(itemCat.getId());
    }

    @Override
    public Integer updateItemCat(ItemCat itemCat) {
        return itemCatMapper.updateById(itemCat);
    }
}
