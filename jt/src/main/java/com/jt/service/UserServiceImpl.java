package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }


    /**
     * 说明: 根据用户名和密码判断数据是否有效
     * 步骤:
     *      1. 将明文进行
     * @param user
     * @return
     */
    @Override
    public String login(User user) {
        //1. 获取原始密码
        String password = user.getPassword();
        //2. 将密码进行加密处理
        String md5Str = DigestUtils.md5DigestAsHex(password.getBytes());
        //3. 将密文传递给对象
        user.setPassword(md5Str);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //queryWrapper.eq("username", user.getUsername())
        //            .eq("password", user.getPassword());
        User userDB = userMapper.selectOne(new QueryWrapper<User>(user));

        String uuid = UUID.randomUUID().toString().replace("-", "");
        return userDB==null?null:uuid;
    }
/*
    @Override
    public PageResult getUserList(PageResult pageResult) {

//        long total = userMapper.selectCount(null);
//
//        int startindex = (pageResult.getPageNum()-1)*pageResult.getPageSize();
//        int pagesize = pageResult.getPageSize();
//        pageResult.setTotal(total).setRows(userMapper.getUserList(startindex,pagesize));
//        return pageResult;
        //1.获取总记录数
        long total = userMapper.selectCount(null);
        //2.获取分页后的结果
        int pageNum = pageResult.getPageNum();  //页数
        int pageSize = pageResult.getPageSize();   //行数
        int startIndex = (pageNum-1) * pageSize;   //起始位置
        List<User> rows = userMapper.getUserList(startIndex,pageSize);
        //List<User> rows = userMapper.getUserList2(pageResult); //利用对象查询
        //将数据进行封装
        pageResult.setTotal(total).setRows(rows); //+2个参数
        return pageResult;  //最终5个参数返回
    }
    */

    @Override
    public PageResult getUserList(PageResult pageResult) {
        //1，定义MP的分页对象
        IPage iPage = new Page(pageResult.getPageNum(),pageResult.getPageSize());
        //2，构建查询条件构造器
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断用户数据是否有效 有效true 无效 false
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"username", pageResult.getQuery());
        iPage = userMapper.selectPage(iPage, queryWrapper);
        long total = iPage.getTotal();
        List<User> rows = iPage.getRecords();
        return pageResult.setTotal(total).setRows(rows);//需要返回的是5个参数
    }

    @Override
    public boolean updataStatus(User user) {
        int flag = userMapper.updateById(user);
        return flag != 0 ? true : false;
    }

    @Override
    public boolean addUser(User user) {
        //1. 获取原始密码
        String password = user.getPassword();
        //2. 将密码进行加密处理
        String md5Str = DigestUtils.md5DigestAsHex(password.getBytes());
        //3. 将密文传递给对象
        user.setPassword(md5Str);
        int flag = userMapper.insert(user);
        return flag != 0 ? true : false;
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.selectOne(new QueryWrapper<User>(new User().setId(id)));
    }

    @Override
    public boolean updateUser(User user) {
        int flag = userMapper.updateById(user);
        return flag != 0 ? true : false;
    }

    @Override
    public boolean deleteUser(Integer id) {
        int flag = userMapper.deleteById(id);
        return flag != 0 ? true : false;
    }
}
