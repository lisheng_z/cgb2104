package com.jt.service;

import com.jt.pojo.ItemCat;

import java.util.List;

public interface ItemCatService {
    List<ItemCat> findItemCatList(Integer level);

    Integer updateStatus(ItemCat itemCat);

    Integer saveItemCat(ItemCat itemCat);

    Integer deleteItemCat(ItemCat itemCat);

    Integer updateItemCat(ItemCat itemCat);

//    List<ItemCat> findItemCatList();
    
    
}
