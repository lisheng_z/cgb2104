package com.jt.service;

import com.jt.vo.ImageVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

@Service
@PropertySource("classpath:/image.properties")
public class FileServiceImpl implements FileService{

    @Value("${file.localDirPath}")
    private String localDirPath;// = "D:/images"
    @Value("${file.preURLPath}")
    private String preURLPath;// = "http://image.jt.com"


    /**
     * 1.验证上传的文件是图片!  jpg|png|gif  采用正则的方式校验
     * 2.防止恶意程序攻击,  验证图片是否有宽度和高度.
     * 3.文件分目录存储     例如: /2021/11/11
     *                    例如2: hash码 8位hash xx/xx/xx/xx
     *  数据hahs时 特征:因为是算法 可能造成数据分配不均!!!
     * 4.防止文件重名,修改文件名称 UUID
     * @param file
     * @return
     */
    @Override
    public ImageVO upload(MultipartFile file) {
        //1.校验图片类型是否正确   正则表达式 a.jpg
        //1.1 获取文件名称
        String fileName = file.getOriginalFilename();
        //1.3将名称全部小写
        fileName = fileName.toLowerCase();
        if(!fileName.matches("^.+\\.(jpg|png|gif)$")){
            //如果文件不是图片 则返回null
            return null;
        }
        try {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            int height = bufferedImage.getHeight();
            int width = bufferedImage.getWidth();
            //3实现分目录存储
            //3.1 按照时间将分配目录  /yyyy/MM/dd/
            if (height == 0 || width == 0){
                return null;
            }
            String dateDirPath = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
            //3.2 "D:/images/yyyy/MM/dd/
            String localDir = localDirPath + dateDirPath;
            File dirFile = new File(localDir);
            if(!dirFile.exists()){//如果目录不存在，则创建目录
                dirFile.mkdirs();
            }
            //4. 防止文件重名  生成UUID
            String uuid = UUID.randomUUID().toString().replace("-", "");
            int index = fileName.lastIndexOf(".");
            //获取后缀   .jpg
            String fileType = fileName.substring(index);
            String realFileName = uuid + fileType;
            String localFilepath = localDir + realFileName;
            file.transferTo(new File(localFilepath));


            String virtualPath = dateDirPath + realFileName;
            String urlPath = preURLPath + virtualPath;
            return new ImageVO(virtualPath,urlPath,realFileName);
        } catch (IOException e) {
            e.printStackTrace();
            //终止程序
            return null;
        }
    }

    @Override
    public void deleteFile(String virtualPath) {
        String path = localDirPath + virtualPath;
        File file = new File(path);
        //实现文件删除
        file.delete();
    }

    /**
     * 步骤:
     *     1.准备文件上传的目录
     *     2.获取文件上传名称
     *     3.拼接文件路径
     *     4.实现文件上传
     *  关于业务层异常处理原则: 将检查异常转化为运行时异常
     * @param file
     */
    /*@Override
    public void upload(MultipartFile file){
        String filePath = "D:/images/";
        File fileDir = new File(filePath);
        //判断目录是否存在
        if(!fileDir.exists()){
            //fileDir.mkdir();  //创建一级目录
            fileDir.mkdirs(); //创建多级目录
        }
        //获取文件名称
        String fileName = file.getOriginalFilename();
        //文件上传目录路径
        String path = filePath + fileName;
        //实现文件上传
        try {
            file.transferTo(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }*/

}
