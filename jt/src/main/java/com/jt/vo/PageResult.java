package com.jt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class PageResult {

    private String query;//用户查询的数据	可以为null
    private Integer pageNum;//查询页数	不能为null
    private  Integer pageSize;//查询条数	不能为null
    private Long total;//查询总记录数	不能为null
    private Object rows;//分页查询的结果	不能为null

}
