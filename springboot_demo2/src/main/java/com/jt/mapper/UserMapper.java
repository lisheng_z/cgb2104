package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

//@Mapper //Spring为该接口创建一个代理对象
public interface UserMapper {
    //查询所有的user表数据
    List<User> findAll();

    //注意事项: 映射文件和注解二选一
    @Select("select * from demo_user where id = #{id}")
    //@Insert("") //新增时使用   "更新"
    //@Update("") //更新
    //@Delete("") //删除
    User findUserById(int i);

    //#{name} 从对象中获取指定的属性的值  #有预编译的效果 防止Sql注入攻击
    @Insert("insert into demo_user(id,name,age,sex) value (null,#{name},#{age},#{sex})")
    void insert(User user);

    @Update("update demo_user set age=#{age},sex=#{sex} where name=#{name}")
    void updateByName(User user);
}
