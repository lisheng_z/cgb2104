package com.jt.pojo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

@Component
@Data
@Accessors(chain = true)//开启链式加载
public class User {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
}
